Summary
=======
This is the DuMuX module containing the code for reproducing the results of

* Weishaupt, Koch & Helmig (2021): [*A fully implicit coupled pore-network/free-flow model for the pore-scale simulation of drying processes*](https://doi.org/10.1080/07373937.2021.1955706)


Installation
============

__Dependencies__
This module [requires](https://dune-project.org/doc/installation/)
* a standard c++17 compliant compiler (e.g. gcc or clang)
* CMake
* pkg-config
* MPI (e.g., OpenMPI)
* Suitesparse (install with `sudo apt-get install libsuitesparse-dev` on Debian/Ubuntu systems)

The easiest way to install this module is to create a new folder and to execute the file
[install_weishaupt2021a.py](https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2021a/-/blob/master/install_weishaupt2021a.py)
in this folder.

```bash
mkdir -p Weishaupt2021a && cd Weishaupt2021a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2021a/-/blob/master/install_weishaupt2021a.py
chmod +x install_weishaupt2021a.py
sh ./install_weishaupt2021a.py
```
```
You may use [ParaView](https://www.paraview.org/) to visualize the results by opening the two `.pvd` files. The `.txt` files contain additional data such as the evaporation rate over time.
```

Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Weishaupt2021a
cd Weishaupt2021a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2021a/-/raw/master/docker_weishaupt2021a.sh
```

Open the Docker Container
```bash
bash docker_weishaupt2021a.sh open
```

After the script has run successfully, you may build the executable

```bash
cd DUMUX/weishaupt2021a/build-cmake
make build_tests
```

It is located in the build-cmake folder according to the following path:

- appl/micromodel_evaporation

It can be executed with an input file, e.g.

```
cd appl/micromodel_evaporation
./micromodel_evaporation_ni_mpnc params/square_still_params.input
```
