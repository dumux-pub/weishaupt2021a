#!/usr/bin/env python3

#
# This installs the module weishaupt2021a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
#
#
# |      module name      |   branch name   |                 commit sha                 |         commit date         |
# |-----------------------|-----------------|--------------------------------------------|-----------------------------|
# |     weishaupt2021a    |  origin/master  |  69025156b1e5dd19c181b458d674886dccf942c6  |  2021-07-21 10:24:07 +0100  |
# |     dune-foamgrid     |  origin/master  |  43bfdb6181fae187fd803eca935a030d8d5ab0bc  |  2021-07-03 20:20:08 +0000  |
# |         dumux         |  origin/master  |  7d845bf04e4d82fa1fd511e3368c18088d0f9852  |  2021-07-20 16:19:56 +0000  |
# |       dune-grid       |  origin/master  |  b803a984737cb09c85b27274a5746fbadbd4b9e0  |  2021-07-18 18:43:54 +0000  |
# |  dune-localfunctions  |  origin/master  |  1120b8169d0930e75510bcd248077c92f85f45e4  |  2021-07-18 21:44:13 +0000  |
# |       dune-istl       |  origin/master  |  48d878582f5456fb6b7ef8b2bde1ac1b34f90450  |  2021-07-07 22:14:57 +0000  |
# |     dune-geometry     |  origin/master  |  36986ad84ea4c06c29d99433881bee4fc1704919  |  2021-07-16 17:58:30 +0000  |
# |      dune-subgrid     |  origin/master  |  c0f298d09cd66d5647951d6797cdb1524683abae  |  2021-04-20 06:54:40 +0000  |
# |      dune-common      |  origin/master  |  57b1eb2ef08aeafdd134fddae45ad190b1bc7b73  |  2021-07-18 21:29:50 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(f'Skip cloning {url} since target folder "{targetFolder}" already exists.')


print("Installing weishaupt2021a")
installModule("weishaupt2021a", "https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2021a.git", "origin/master", "69025156b1e5dd19c181b458d674886dccf942c6")

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "43bfdb6181fae187fd803eca935a030d8d5ab0bc")

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/master", "7d845bf04e4d82fa1fd511e3368c18088d0f9852")

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/master", "b803a984737cb09c85b27274a5746fbadbd4b9e0")

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/master", "1120b8169d0930e75510bcd248077c92f85f45e4")

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/master", "48d878582f5456fb6b7ef8b2bde1ac1b34f90450")

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/master", "36986ad84ea4c06c29d99433881bee4fc1704919")

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git", "origin/master", "c0f298d09cd66d5647951d6797cdb1524683abae")

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/master", "57b1eb2ef08aeafdd134fddae45ad190b1bc7b73")

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
