// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the 1d-3d embedded mixed-dimension model coupling two
 *        one-phase porous medium flow problems
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

#include <dumux/porenetwork/common/pnmvtkoutputmodule.hh>
#include <dumux/porenetwork/common/utilities.hh>
#include <dumux/multidomain/modifiednewtonsolver.hh>
#include <dumux/multidomain/boundary/pnmstokes/couplingmanager.hh>
#include <dumux/io/grid/snappygridcreator.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/porenetwork/gridmanager.hh>

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include "staggerednewtonconvergencewriter.hh"

#include "properties.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    if (mpiHelper.rank() == 0)
        Parameters::print();

    // Define the sub problem type tags
    using BulkTypeTag = Properties::TTag::FreeFlowTypeTag;
    using LowDimTypeTag = Properties::TTag::PNMTwoPTypeTag;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using LowDimGridManager = Dumux::PoreNetwork::GridManager<2>;
    LowDimGridManager lowDimGridManager;
    lowDimGridManager.init("PNM"); // pass parameter group

    using BulkGridManager = Dumux::SnappyGridCreator<2, LowDimGridManager>;
    BulkGridManager bulkGridManager;
    auto data = bulkGridManager.init(lowDimGridManager.grid(), *(lowDimGridManager.getGridData()), "Stokes");
    const auto auxiliaryPositions = data.interFacePositions[0/*dimIdx*/].value();

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGridManager.grid().leafGridView();
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();
    auto lowDimGridData = lowDimGridManager.getGridData();

    // create the finite volume grid geometry
    using BulkGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    bulkGridGeometry->update();
    using LowDimGridGeometry = GetPropType<LowDimTypeTag, Properties::GridGeometry>;
    auto lowDimGridGeometry = std::make_shared<LowDimGridGeometry>(lowDimGridView);
    lowDimGridGeometry->update(*lowDimGridData);

    // the mixed dimension type traits
    using Traits = StaggeredMultiDomainTraits<BulkTypeTag, BulkTypeTag, LowDimTypeTag>;

    // the coupling manager
    using CouplingManager = PNMStokesCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the indices
    constexpr auto bulkCellCenterIdx = CouplingManager::bulkCellCenterIdx;
    constexpr auto bulkFaceIdx = CouplingManager::bulkFaceIdx;
    constexpr auto lowDimIdx = CouplingManager::lowDimIdx;

    GetPropType<BulkTypeTag, Properties::FluidSystem>::init();

    // get some time loop parameters
    using Scalar = GetPropType<BulkTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");
    const Scalar dtOutput = getParam<Scalar>("TimeLoop.DtOutput", 0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    if (dtOutput > 0.0)
        timeLoop->setPeriodicCheckPoint(dtOutput);

    // the problem (initial and boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    auto bulkProblem = std::make_shared<BulkProblem>(bulkGridGeometry, couplingManager);

    // the spatial parameters
    using LowDimSpatialParams = GetPropType<LowDimTypeTag, Properties::SpatialParams>;
    auto lowDimspatialParams = std::make_shared<LowDimSpatialParams>(lowDimGridGeometry);

    using LowDimProblem = GetPropType<LowDimTypeTag, Properties::Problem>;
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimGridGeometry, lowDimspatialParams, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[bulkCellCenterIdx].resize(bulkGridGeometry->numCellCenterDofs());
    sol[bulkFaceIdx].resize(bulkGridGeometry->numFaceDofs());
    sol[lowDimIdx].resize(lowDimGridGeometry->numDofs());

    auto bulkSol = partial(sol, bulkFaceIdx, bulkCellCenterIdx);

    lowDimProblem->applyInitialSolution(sol[lowDimIdx]);
    bulkProblem->applyInitialSolution(bulkSol);

    auto solOld = sol;

    couplingManager->init(bulkProblem, lowDimProblem, sol);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkGridGeometry);
    bulkGridVariables->init(bulkSol);
    using LowDimGridVariables = GetPropType<LowDimTypeTag, Properties::GridVariables>;
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // pass the grid variables to the coupling manager
    couplingManager->setGridVariables(std::make_tuple(bulkGridVariables->faceGridVariablesPtr(),
                                                      bulkGridVariables->cellCenterGridVariablesPtr(),
                                                      lowDimGridVariables));

    FluxOverSurface<BulkGridVariables,
                    decltype(bulkSol),
                    GetPropType<BulkTypeTag, Properties::ModelTraits>,
                    GetPropType<BulkTypeTag, Properties::LocalResidual>> flux(*bulkGridVariables, bulkSol);
    bulkProblem->setPlanes(flux, auxiliaryPositions);

    PoreNetwork::AveragedValues<LowDimGridVariables, decltype(sol[lowDimIdx])> avgValues(*lowDimGridVariables, sol[lowDimIdx]);
    using FS = typename LowDimGridVariables::VolumeVariables::FluidSystem;
    avgValues.addAveragedQuantity([](const auto& v){ return v.saturation(FS::phase0Idx); }, [](const auto& v){ return v.poreVolume(); }, "avgSat");

    // helper lambda to get the water mass within the PNM
    auto totalWaterMassPNM = [&]()
    {
        auto fvGeometry = localView(*lowDimGridGeometry);
        auto elemVolVars = localView(lowDimGridVariables->curGridVolVars());
        std::vector<bool> poreVisited(lowDimGridGeometry->numDofs(), false);
        Scalar result = 0.0;

        for (const auto& element : elements(lowDimGridView))
        {
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, sol[lowDimIdx]);

            for (int scvIdx = 0; scvIdx < fvGeometry.numScv(); ++scvIdx)
            {
                static constexpr auto dofCodim = std::decay_t<decltype(lowDimGridView)>::dimension;
                const auto dofIdxGlobal = lowDimGridGeometry->vertexMapper().subIndex(element, scvIdx, dofCodim);

                if (poreVisited[dofIdxGlobal])
                    continue;
                else
                {
                    const auto& volVars = elemVolVars[scvIdx];
                    using FluidSystem = typename std::decay_t<decltype(volVars)>::FluidSystem;
                    for (int phaseIdx = 0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
                    {
                        result += volVars.massFraction(phaseIdx, FluidSystem::H2OIdx)*volVars.density(phaseIdx)
                                  * volVars.poreVolume() * volVars.saturation(phaseIdx);
                    }
                    poreVisited[dofIdxGlobal] = true;
                }
            }
        }
        return result;
    };

    // intialize the vtk output module
#if ISOTHERMAL
    const auto suffix_energy = "";
#else
    const auto suffix_energy = "_ni";
#endif
#if USEMPNC
    const auto suffix_model = "_mpnc";
#else
    const auto suffix_model = "";
#endif
    const auto bulkName = getParam<std::string>("Problem.Name") + "_" + bulkProblem->name() + suffix_energy + suffix_model;
    const auto lowDimName = getParam<std::string>("Problem.Name") + "_" + lowDimProblem->name() + suffix_energy + suffix_model;

    // returns the relative humidity for the free flow domain
    auto rh = [](const auto& v)
    {
        return v.fluidState().partialPressure(0, 1)
               / std::decay_t<decltype(v)>::FluidSystem::MultiPhaseFluidSystem::H2O::vaporPressure(v.temperature());
    };

    StaggeredVtkOutputModule<BulkGridVariables, decltype(bulkSol)> bulkVtkWriter(*bulkGridVariables, bulkSol, bulkName);
    GetPropType<BulkTypeTag, Properties::IOFields>::initOutputModule(bulkVtkWriter);
    bulkVtkWriter.addVolumeVariable([](const auto& v){ return v.pressure()-1e5; }, "delP");
    bulkVtkWriter.addVolumeVariable(rh, "rH");
    bulkVtkWriter.write(0.0);

    PoreNetwork::VtkOutputModule<LowDimGridVariables,
                                 GetPropType<LowDimTypeTag, Properties::FluxVariables>,
                                 std::decay_t<decltype(sol[lowDimIdx])>> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimName);
    lowDimVtkWriter.addVolumeVariable([](const auto& v){ return v.pressure(1)-1e5; }, "delPN");
    lowDimVtkWriter.addVolumeVariable([](const auto& v){ return v.pressure(0)-1e5; }, "delPW");
    lowDimVtkWriter.addVolumeVariable([](const auto& v){ return v.surfaceTension(); }, "surfaceTension");
    lowDimVtkWriter.addVolumeVariable([](const auto& v){ return std::decay_t<decltype(v)>::FluidSystem::relativeHumidity(v.fluidState()); } , "relativeHumidity");
    GetPropType<LowDimTypeTag, Properties::IOFields>::initOutputModule(lowDimVtkWriter);

    lowDimVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& fluxVarsCache) { return fluxVars.molecularDiffusionFlux(0)[1]; }, "diffusiveFlux_0^1");
    lowDimVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& fluxVarsCache) { return fluxVars.molecularDiffusionFlux(1)[0]; }, "diffusiveFlux_1^0");
    lowDimVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& fluxVarsCache) { return fluxVars.heatConductionFlux(); }, "heatConductionFlux");
    lowDimVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& fluxVarsCache) { return fluxVarsCache.throatCrossSectionalArea(0); }, "throatCrossSectionalArea0");
    lowDimVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& fluxVarsCache) { return fluxVarsCache.throatCrossSectionalArea(1); }, "throatCrossSectionalArea1");

    lowDimVtkWriter.addField(lowDimGridGeometry->poreVolume(), "poreVolume", Vtk::FieldType::vertex);
    lowDimVtkWriter.write(0.0);

    // log the initial water content
    avgValues.eval();
    Scalar waterMassOld = totalWaterMassPNM();
    lowDimProblem->postTimeStep(0.0/*time*/, waterMassOld, avgValues["avgSat"], 0.0/*evaporationRate*/);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkProblem, bulkProblem, lowDimProblem),
                                                 std::make_tuple(bulkGridGeometry->faceFVGridGeometryPtr(),
                                                                 bulkGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 lowDimGridGeometry),
                                                 std::make_tuple(bulkGridVariables->faceGridVariablesPtr(),
                                                                 bulkGridVariables->cellCenterGridVariablesPtr(),
                                                                 lowDimGridVariables),
                                                 couplingManager,
                                                 timeLoop, solOld);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    const auto& constSol = sol;
    const auto constBulkSol = partial(constSol, bulkFaceIdx, bulkCellCenterIdx);

    using ConvWriter = MultiDomainStaggeredNewtonConvergenceWriter<typename Traits::SolutionVector,
                                                                   BulkGridGeometry, std::decay_t<decltype(constBulkSol)>,
                                                                   LowDimGridGeometry, std::decay_t<decltype(sol[lowDimIdx])>>;

    auto convWriter = std::make_shared<ConvWriter>(*bulkGridGeometry, *lowDimGridGeometry);


    // the non-linear solver
    using NewtonSolver = ModifiedMultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->start(); do
    {
        // reset the convergence writer so that each time can be easily identified in the pvd file
        convWriter->reset(timeLoop->timeStepIndex());

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(solOld);

        lowDimProblem->setTime(timeLoop->time()+timeLoop->timeStepSize());

        // write convergence for given time interval
        static const auto convWriterBounds = getParam<std::array<int, 2>>("Newton.ConvergenceWriterTimeInterval", std::array{0,0});
        if (timeLoop->timeStepIndex() > convWriterBounds[0] && timeLoop->timeStepIndex() < convWriterBounds[1])
            nonLinearSolver.attachConvergenceWriter(convWriter);
        else
            nonLinearSolver.detachConvergenceWriter();

        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;
        bulkGridVariables->advanceTimeStep();
        lowDimGridVariables->advanceTimeStep();

        flux.calculateMassOrMoleFluxes();
        bulkProblem->printFluxes(flux, timeLoop->time() + timeLoop->timeStepSize());

        // log the water content
        avgValues.eval();
        const Scalar waterMassNew = totalWaterMassPNM();
        const Scalar rate = (waterMassNew - waterMassOld) / timeLoop->timeStepSize();
        lowDimProblem->postTimeStep(timeLoop->time() + timeLoop->timeStepSize(), waterMassNew, avgValues["avgSat"], rate);
        waterMassOld = waterMassNew;

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        static const bool alwaysWriteOutput = getParam<bool>("Vtk.AlwaysWriteOutput", false);
        if (timeLoop->isCheckPoint() || alwaysWriteOutput || lowDimGridVariables->gridFluxVarsCache().invasionState().hasChanged())
        {
            bulkVtkWriter.write(timeLoop->time());
            lowDimVtkWriter.write(timeLoop->time());
        }

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

        if (avgValues["avgSat"] < 1e-14)
            timeLoop->setFinished();

    } while (!timeLoop->finished());

    timeLoop->finalize(bulkGridView.comm());
    timeLoop->finalize(lowDimGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
