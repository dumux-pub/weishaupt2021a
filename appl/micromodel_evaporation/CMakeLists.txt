add_input_file_links()
dune_symlink_to_source_files(FILES run_sim.py params)

dune_add_test(NAME micromodel_evaporation_ni_mpnc
              SOURCES main.cc
              COMPILE_DEFINITIONS ISOTHERMAL=0 USEMPNC=1 USEVARIABLESURFACETENSION=1)
