[TimeLoop]
TEnd = 86400e3 # [s]
DtInitial = 1e-3 # [s]
MaxTimeStepSize = 300 # [s]
DtOutput = 300 # [s]

[PNM.Problem]
InitialSaturation = 1.0 # [-]
InitialTemperature = 293.15 # [K]
Name = pnm
InitialSaturationTop = 0.99 # [K]

[PNM.Grid]
PoreGeometry = Cube
ThroatCrossSectionShape = Circle
LowerLeft = 0.25e-2 0.5e-2 # [m]
UpperRight = 1.25e-2 1e-2 # [m]
NumPores = 19 10 # number of pores in x- and y-direction
MeanPoreInscribedRadius = 200e-6 # inscribed pore body radius [m]
StandardDeviationPoreInscribedRadius = 0# 40e-6 # [m], results in uniform network
DeletionProbability = 0 0 1 1 # delete all throats that are not axis-aligned
ThroatRadiusN = 0.1 # relates pore body radius to throat radius
ParameterType = lognormal # pore size distribution, has no effect here
SubstractRadiiFromThroatLength = true
ParameterRandomNumberSeed = 1
RemoveThroatsOnBoundary = 3
PriorityList = 3 2 1 0
BoundaryFaceMarker = 1 1 1 2

CapPoresOnBoundaries = 3 # halves the volume of the pore bodies a the interface
AddThroatVolumeToPoreVolume = true

Subregion0.LowerLeft = 0.25e-2 0
Subregion0.UpperRight = 0.0075 1e-2
Subregion0.MeanPoreInscribedRadius = 220e-6
Subregion0.StandardDeviationPoreInscribedRadius = 0 #40e-6
NumSubregions = 1

[PNM.Assembly]
NumericDifferenceMethod = 0

[Stokes.Problem]
DeltaP = 0.0 # [Pa], pressure difference between inlet and outlet
OpenTop = true # flow from left to right
Name = stokes
EnableInertiaTerms = true # solve Navier-Stokes equations
InitialRelativeHumidity = 1.0 # [-]
BoundaryRelativeHumidity = 0.2 # [-]
ConsiderWallFriction = true # Flekkoy wall friciton term
UseSlipCondition = true # use novel pore-scale slip condition
EnableCoupling = true # enbale PNM/free-flow coupling
Height = 400e-6 # [m] virtual domain height
PseudoWallFractionFactor = 12
BetaFactor = 3.71
BetaVerbose = false

[Stokes.Grid]
LowerLeft = 0 1e-2 # [m]
UpperRight = 1.5e-2 1.5e-2 # [m]
UpstreamCells0 = 20 # no. cells left of pore network
CellsPerThroat = 3 # use 3 cells per throat width
DownstreamCells0 = 20 # no- cell right of pore network
CoupleOverPoreRadius = true
Grading1 = 1.2 1.1 -1.1 -1.2
Positions1 = 1.1e-2 1.25e-2 1.4e-2
Cells1 = 12 4 4 12

[Stokes.Assembly]
NumericDifference.BaseEpsilon = 1e-10

[Newton]
PlausibilityCheck = false
MaxRelativeShift = 1e-6
AllowedSaturationChange = 0.9
MaxSteps = 15
RetryTimeStepReductionFactor = 0.2
EnableChop = true
TemperatureClampValues = 275 300
NumChoppedUpdates = 1
PressureMaxDelta = 1e1
SaturationMaxDelta = 0.1
TemperatureMaxDelta = 0.1
MaxTimeStepDivisions = 20
MaxAbsoluteResidual = 1e-5
EnableAbsoluteResidualCriterion = true

FirstTransFactor = 1
TransfactorProgression = 0
InvasionEventReductionFactor = 1

[Component]
SolidHeatCapacity = 1 # for compatibility, not actually used
SolidDensity = 1 # for compatibility, not actually used
SolidThermalConductivity = 1 # for compatibility, not actually used

[Problem]
Name = evaporation
EnableGravity = false
OnlyDiffusion = false

[Vtk]
AddVelocity = true
WriteFaceData = false
AlwaysWriteOutput = false

[FluxOverSurface]
Verbose = false

[InvasionState]
AccuracyCriterion = 0.90

[PrimaryVariableSwitch]
Verbosity = 2

[MPNC]
ThresholdSw = 0.1
FisherBurmeister = true
RegularizedFisherBurmeisterAlpha = 0.95

[SpatialParams]
HighSwRegularizationMethod = Linear
RegularizationHighSw = 0.999999

[LinearSolver]
Verbosity = 0

