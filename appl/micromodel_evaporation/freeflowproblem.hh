// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Channel flow setup for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_TEST_PROBLEM_HH
#define DUMUX_CHANNEL_TEST_PROBLEM_HH

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

namespace Dumux
{
template <class TypeTag>
class ChannelTestProblem;

/*!
 * \brief  Problem for the one-phase (Navier-) Stokes problem in a channel.
 */
template <class TypeTag>
class ChannelTestProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using FluidSystem = typename GetPropType<TypeTag, Properties::VolumeVariables>::FluidSystem;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    ChannelTestProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "Stokes"), eps_(1e-6), couplingManager_(couplingManager)
    {
        deltaP_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaP", 0.0);
        openTopScenario_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.OpenTop", false);
        boundaryTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.BoundaryTemperature", 273.15 + 20.0);

        const Scalar initalRelativeHumidity = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialRelativeHumidity", -1.0);
        if (initalRelativeHumidity >= 0.0)
        {
            const Scalar pVap = FluidSystem::MultiPhaseFluidSystem::H2O::vaporPressure(temperature());
            initialMoleFraction_ = initalRelativeHumidity * pVap / 1e5;
        }
        else
            initialMoleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialMoleFraction");

        const Scalar boundaryRelativeHumidity = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.BoundaryRelativeHumidity", -1.0);
        if (boundaryRelativeHumidity >= 0.0)
        {
            const Scalar pVap = FluidSystem::MultiPhaseFluidSystem::H2O::vaporPressure(temperature());
            boundaryMoleFraction_ = boundaryRelativeHumidity * pVap / 1e5;
        }
        else
            boundaryMoleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.BoundaryMoleFraction");

        considerWallFriction_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ConsiderWallFriction", false);
        height_ =  getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Height", 200e-6);
        extrusionFactor_ = considerWallFriction_ ? height_ : 1.0;
        useSlipCondition_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseSlipCondition", false);
        enableCoupling_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableCoupling", true);

        logfile_.open("logfile_" + this->name() + ".txt");
    }

    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor_;  }

    /*!
     * \brief Returns the velocity in the porous medium.
     */
    GlobalPosition porousMediumVelocity(const Element& element, const SubControlVolumeFace& scvf) const
    {
        if (couplingManager().isCoupledEntity(CouplingManager::bulkFaceIdx, scvf))
            return couplingManager().couplingData().boundaryVelocity(element, scvf);
        else
            return GlobalPosition(0.0);
    }

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolumeFace& scvf) const
    {
        auto source = NumEqVector(0.0);

        if (considerWallFriction_)
        {
            static const Scalar factor = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PseudoWallFractionFactor", 12.0);
            source[scvf.directionIndex()] = this->pseudo3DWallFriction(scvf, elemVolVars, elemFaceVars, height_, factor);
        }

        return source;
    }

    /*!
     * \brief Returns the beta value required as input parameter for the
     *        pore-scale slip condition.
     */
    Scalar betaBJ(const Element& element, const SubControlVolumeFace& scvf, const GlobalPosition& tangentialVector) const
    {
        const Scalar radius = couplingManager().couplingData().coupledRadius(element, scvf);
        static const bool betaVerbose = getParamFromGroup<bool>(this->paramGroup(), "Problem.BetaVerbose", false);
        static const Scalar betafactor = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.BetaFactor");

        if (betaVerbose)
            std::cout << "Beta at " << scvf.center() << ", radius: " << radius << ", BetaFactor: " <<  betafactor << ", result: " << betafactor / radius << std::endl;

        return betafactor / radius;
    }

   /*!
     * \name Problem parameters
     */
    // \{

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return boundaryTemperature_; }


    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        if (openTopScenario_)
            return boundaryTypesForOpenTopScenario_(element, scvf);
        else
            return boundaryTypesForChannelScenario_(element, scvf);
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if(enableCoupling_ && couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            const auto tmp = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[Indices::conti0EqIdx] = tmp[0];
            values[Indices::conti0EqIdx + 1] = tmp[1];
            values[scvf.directionIndex()] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
        }
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param element The element
     * \param scvf The subcontrolvolume face
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos = scvf.dofPosition();
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1e5;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;
        values[Indices::temperatureIdx] = boundaryTemperature_;

        if (isInlet_(globalPos))
        {
            values[Indices::pressureIdx] = 1e5 + deltaP_;
            values[Indices::conti0EqIdx + 1] = boundaryMoleFraction_;
        }

        if (openTopScenario_&& isTop_(globalPos))
            values[Indices::conti0EqIdx + 1] = boundaryMoleFraction_;

        if (enableCoupling_ && couplingManager().isCoupledEntity(CouplingManager::bulkFaceIdx, scvf))
        {
            const auto couplingVelocity = couplingManager().couplingData().boundaryVelocity(element, scvf);
            values[Indices::velocityXIdx] = couplingVelocity[0];
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param element The element
     * \param scvf The subcontrolvolume face
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    { return PrimaryVariables(0.0); }

    // \}

    template<class FluxOverPlane>
    void setPlanes(FluxOverPlane& flux, const std::vector<Scalar>& auxiliaryPositions)
    {
        const Scalar xMax = this->gridGeometry().bBoxMax()[0];
        const Scalar yMin = this->gridGeometry().bBoxMin()[1];
        const Scalar yMax = this->gridGeometry().bBoxMax()[1];

        const GlobalPosition pBottom{xMax, yMin};
        const GlobalPosition pTop{xMax, yMax};
        flux.addSurface("outlet", pBottom, pTop);

        for(auto i : auxiliaryPositions)
            std::cout << i << std::endl;

        logfile_ << std::left << std::setw(20) << std::setfill(' ') << "Time";

        for (int i = 0; i < auxiliaryPositions.size(); i+=2)
        {
            const GlobalPosition pLeft{auxiliaryPositions[i] , yMin};
            const GlobalPosition pRight{auxiliaryPositions[i+1] , yMin};
            flux.addSurface("throatsVerticalFlux", pLeft, pRight);
            logfile_ << std::left << std::setw(20) << std::setfill(' ') << "fluxAir" + std::to_string(i/2)
                     << std::left << std::setw(20) << std::setfill(' ') << "fluxWater_" + std::to_string(i/2)
                     << std::left << std::setw(20) << std::setfill(' ') << "fluxEnergy_" + std::to_string(i/2);
        }
        logfile_ << std::left << std::setw(20) << std::setfill(' ') << "fluxAir_total"
                 << std::left << std::setw(20) << std::setfill(' ') << "fluxWater_total"
                 << std::left << std::setw(20) << std::setfill(' ') << "fluxEnergy_total"
                 << std::endl;
    }

    template<class FluxOverPlane>
    void printFluxes(const FluxOverPlane& flux, Scalar time)
    {
        static const auto word = ModelTraits::useMoles() ? "mole" : "mass";
        std::cout << word << " flux at outlet is: " << flux.netFlux("outlet") << std::endl;

        const auto& values = flux.values("throatsVerticalFlux");
        logfile_ << time << " ";

        for (int i = 0; i < values.size(); ++i)
        {
            auto tmp = values[i];
            if (ModelTraits::useMoles())
            {
                tmp[0] *= FluidSystem::molarMass(0);
                tmp[1] *= FluidSystem::molarMass(1);
            }
            std::cout << "throat " << i << " : " << std::setprecision(15) <<  tmp << std::endl;
            logfile_ << std::fixed << std::scientific <<  tmp << " ";
        }

        if (ModelTraits::useMoles())
        {
            auto tmp = flux.netFlux("throatsVerticalFlux");
            tmp[0] *= FluidSystem::molarMass(0);
            tmp[1] *= FluidSystem::molarMass(1);
            logfile_ << std::fixed << std::scientific << tmp << std::endl;
            std::cout << "\nvertical net flux: "  << tmp << std::endl;
        }
        else
        {
            logfile_  << std::fixed << std::scientific << flux.netFlux("throatsVerticalFlux") << std::endl;
            std::cout << "\nvertical net flux: "  << flux.netFlux("throatsVerticalFlux") << std::endl;
        }

        std::cout << "\n##################################\n" << std::endl;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1e+5;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;
        values[Indices::conti0EqIdx + 1] = initialMoleFraction_;
        values[Indices::temperatureIdx] = boundaryTemperature_;

        return values;
    }

    // \}

private:

    BoundaryTypes boundaryTypesForOpenTopScenario_(const Element& element,
                                                   const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();

       if (enableCoupling_ && couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            if (useSlipCondition_)
                values.setBeaversJoseph(Indices::velocityXIdx);
            else
                values.setCouplingDirichlet(Indices::velocityXIdx);

            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx+1);
            values.setCouplingNeumann(Indices::energyEqIdx);
        }
        else if (isTop_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
            values.setDirichlet(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::temperatureIdx);
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setNeumann(Indices::conti0EqIdx);
            values.setNeumann(Indices::conti0EqIdx + 1);
            values.setNeumann(Indices::energyEqIdx);
        }
        return values;
    }

    BoundaryTypes boundaryTypesForChannelScenario_(const Element& element,
                                                   const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();

        if (isInlet_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
            values.setDirichlet(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::temperatureIdx);
        }
        else if (isOutlet_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
            values.setOutflow(Indices::conti0EqIdx + 1);
            values.setOutflow(Indices::energyEqIdx);
        }
        else if (enableCoupling_ && couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            if (useSlipCondition_)
                values.setBeaversJoseph(Indices::velocityXIdx);
            else
                values.setCouplingDirichlet(Indices::velocityXIdx);

            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx+1);
            values.setCouplingNeumann(Indices::energyEqIdx);
        }
        else // no flow
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setNeumann(Indices::conti0EqIdx);
            values.setNeumann(Indices::conti0EqIdx+1);
            values.setNeumann(Indices::energyEqIdx);
        }

        return values;
    }

    bool isInlet_(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool isOutlet_(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool isTop_(const GlobalPosition& globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }


    Scalar eps_;
    Scalar deltaP_;
    Scalar initialMoleFraction_;
    Scalar boundaryMoleFraction_;
    bool openTopScenario_;

    Scalar height_;
    bool considerWallFriction_;
    bool useSlipCondition_;
    Scalar extrusionFactor_;
    Scalar boundaryTemperature_;

    bool enableCoupling_;
    std::ofstream logfile_;

    std::shared_ptr<CouplingManager> couplingManager_;
};
} //end namespace

#endif
