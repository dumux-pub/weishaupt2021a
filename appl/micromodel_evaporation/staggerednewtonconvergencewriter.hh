// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Nonlinear
 * \brief This class provides the infrastructure to write the
 *        convergence behaviour of the Newton method for
 *        multidomain simulations into a VTK file.
 */
#ifndef DUMUX_MULTIDOMAIN_STAGGERED_NEWTON_CONVERGENCE_WRITER_HH
#define DUMUX_MULTIDOMAIN_STAGGERED_NEWTON_CONVERGENCE_WRITER_HH

#include <string>
#include <dune/common/hybridutilities.hh>
#include <dumux/nonlinear/newtonconvergencewriter.hh>
#include <dumux/nonlinear/staggerednewtonconvergencewriter.hh>

namespace Dumux {

/*!
 * \ingroup Nonlinear
 * \brief Writes the intermediate solutions for every Newton iteration
 * \note To use this create a shared_ptr to an instance of this class in the main file
 *       and pass it to newton.solve(x, convergencewriter). You can use the reset method
 *       to write out multiple Newton solves with a unique id, if you don't call use all
 *       Newton iterations just come after each other in the pvd file.
 */
template <class SolutionVector,
          class StaggeredGridGeometry, class StaggeredSolutionVector,
          class OtherGridGeometry, class OtherSolutionVector>
class MultiDomainStaggeredNewtonConvergenceWriter : public ConvergenceWriterInterface<SolutionVector>
{
    using StaggeredNewtonConvergenceWriter = Dumux::StaggeredNewtonConvergenceWriter<StaggeredGridGeometry, StaggeredSolutionVector>;
    using OtherNewtonConvergenceWriter = Dumux::NewtonConvergenceWriter<OtherGridGeometry, OtherSolutionVector>;

public:
    /*!
     * \brief Constructor
     * \param gridGeometryTuple A tuple of grid geometries
     * \param name Base name of the vtk output
     */
    MultiDomainStaggeredNewtonConvergenceWriter(const StaggeredGridGeometry& staggeredGridGeometry,
                                                const OtherGridGeometry& otherGridGeometry,
                                                const std::string& name = "newton_convergence")
    : staggeredConvWriter_(staggeredGridGeometry, name + "_staggered")
    , otherNewtonConvergenceWriter_(otherGridGeometry, name)
    {}

    // ! Resizes the output fields. This has to be called whenever the grid changes
    void resize()
    {
        staggeredConvWriter_.resize();
        otherNewtonConvergenceWriter_.resize();
    }

    //! Reset the convergence writer for a possible next Newton step
    //! You may set a different id in case you don't want the output to be overwritten by the next step
    void reset(std::size_t newId = 0UL)
    {
        staggeredConvWriter_.reset(newId);
        otherNewtonConvergenceWriter_.reset(newId);
    }

    void write(const SolutionVector& uLastIter,
               const SolutionVector& deltaU,
               const SolutionVector& residual) override
    {

        const auto staggeredLastIter = partial(uLastIter, Dune::index_constant<0>(), Dune::index_constant<1>());
        const auto staggeredDeltaU = partial(deltaU, Dune::index_constant<0>(), Dune::index_constant<1>());
        const auto staggeredResidual = partial(residual, Dune::index_constant<0>(), Dune::index_constant<1>());
        staggeredConvWriter_.write(staggeredLastIter, staggeredDeltaU, staggeredResidual);

        otherNewtonConvergenceWriter_.write(uLastIter[Dune::index_constant<2>()], deltaU[Dune::index_constant<2>()], residual[Dune::index_constant<2>()]);
    }

private:
    StaggeredNewtonConvergenceWriter staggeredConvWriter_;
    OtherNewtonConvergenceWriter otherNewtonConvergenceWriter_;
};

} // end namespace Dumux

#endif
