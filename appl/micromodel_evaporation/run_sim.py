#!/usr/bin/env python3

import os
import subprocess
import argparse

def run(args):

    executable = "micromodel_evaporation_ni_mpnc"

    executable_dir = os.getcwd()
    simulation_dir =  os.path.abspath(executable_dir + '/' + args['path'] + '/' + 'dP_' + str(args['deltaP']) + '_tB_' + str(args['temperatureBoundary']) + '_rhB_' + str(args['relativeHumidityBoundary']) + '_tG_' + str(args['throatGeometry']) + '_numPores_' + str(args['numPoresX']) + '/')

    print("sim dir is ", simulation_dir)

    if args["createDirectory"] and not os.path.exists(simulation_dir):
        print("creating new directory")
        os.makedirs(simulation_dir)
    elif not args["createDirectory"] and not os.path.exists(simulation_dir):
        raise OSError("Directory does not exist yet! Use --create")
    elif args["createDirectory"] and os.path.exists(simulation_dir):
        raise OSError("Directory already exists! Delete old dir first!")

    if args["parameters"] is None:
        params_file = "../params/paramsforpythonscript.input"
    else:
        params_file = os.path.realpath(args["parameters"].name)

    command = executable_dir + "/" + executable + " " + params_file

    command += ' -PNM.Grid.StandardDeviationPoreInscribedRadius ' + str(args["standardDeviation"])
    command += ' -PNM.Grid.ThroatCrossSectionShape ' + str(args['throatGeometry'])
    command += ' -PNM.Grid.NumPores "' + str(args['numPoresX']) + ' 10"'
    command += ' -Stokes.Problem.DeltaP ' + str(args['deltaP'])
    command += ' -Stokes.Problem.OpenTop '  + ('true' if abs(args['deltaP']) < 1e-15 else 'false')
    command += ' -Stokes.Problem.BoundaryRelativeHumidity ' + str(args['relativeHumidityBoundary'])
    command += ' -Stokes.Problem.BoundaryTemperature ' + str(args['temperatureBoundary'])

    if args['noSlurm']:
        command += ' > results.log  2> .errors.err &'
    else:
        file = """#!/bin/bash
#SBATCH -n1 -J pnm_evp
#SBATCH --error=errors.err
#SBATCH --output=results.log
umask 022
"""
        file += command
        file += '\nexit 0'

        with open(simulation_dir + '/start.sh', "w+") as a:
            a.write(file)

    print(command)

    subprocess.Popen('sbatch start.sh', cwd=simulation_dir, shell=True)

parser = argparse.ArgumentParser()
parser.add_argument('-dP', '--deltaP', type=float, required=True)
parser.add_argument('-tB', '--temperatureBoundary', type=float, default=273.15+20.0)
parser.add_argument('-rhB', '--relativeHumidityBoundary', type=float, default=0.2)
parser.add_argument('-nPX', '--numPoresX', type=int, default=19)
parser.add_argument('-ns', '--noSlurm', action='store_true')
parser.add_argument('-create', '--createDirectory', action='store_true')
parser.add_argument('-p', '--path', type=str, required=False, default="", help='the relative path for the simulation results')
parser.add_argument('-params', '--parameters', type=argparse.FileType('r', encoding='UTF-8'), help='parameter file (relative path)')
parser.add_argument('-tG', '--throatGeometry', type=str, required=True, help='throat geometry')
parser.add_argument('-stddev', '--standardDeviation', type=float, required=False, default=0.0, help='pore radius std dev')
args = vars(parser.parse_args())

run(args)
