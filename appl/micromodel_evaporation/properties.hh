// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief The properties
 */

#ifndef DUMUX_EVAPORATION_PROPERTIES_HH
#define DUMUX_EVAPORATION_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidmatrixinteractions/mp/mpadapter.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>
#include <dumux/discretization/box.hh>
#include <dumux/porenetwork/mpnc/model.hh>

#include "pnmproblem.hh"
#include "freeflowproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct PNMTwoPTypeTag { using InheritsFrom = std::tuple<PNMMPNCNI>; };
struct FreeFlowTypeTag { using InheritsFrom = std::tuple<NavierStokesNCNI, StaggeredFreeFlowModel>; };

} // end namespace TTag

template<class TypeTag>
struct FluidSystemHelper
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2Otype = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using Policy = FluidSystems::H2OAirDefaultPolicy<false/*fastButSimplifiedRelations*/>;
    using type = FluidSystems::H2OAir<Scalar, H2Otype, Policy, true/*useKelvinVaporPressure*/>;
};

// PNM properties
// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PNMTwoPTypeTag> { using type = Dumux::PNMOnePProblem<TypeTag>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PNMTwoPTypeTag> { using type = Dune::FoamGrid<1, 2>; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr int value = 3; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PNMTwoPTypeTag>
{ using type = typename FluidSystemHelper<TypeTag>::type; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PNMTwoPTypeTag>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using LocalRules = Dumux::PoreNetwork::FluidMatrix::MultiShapeTwoPLocalRules<Scalar>;
public:
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using type = Dumux::TwoPMPNCSpatialParams<GridGeometry, Scalar, LocalRules, Indices::temperatureIdx>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PNMTwoPTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeFlowTypeTag, Properties::TTag::FreeFlowTypeTag, TypeTag>;
    using type = Dumux::PNMStokesCouplingManager<Traits>;
};


// Free-flow properties
// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::FreeFlowTypeTag> { using type = Dumux::ChannelTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::FreeFlowTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::FreeFlowTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::FreeFlowTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::FreeFlowTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::FreeFlowTypeTag> { static constexpr int value = 3; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FreeFlowTypeTag>
{
    using FS = typename FluidSystemHelper<TypeTag>::type;
    static constexpr auto phaseIdx = FS::gasPhaseIdx; // simulate the water phase
    using type = FluidSystems::OnePAdapter<FS, phaseIdx>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeFlowTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeFlowTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PNMTwoPTypeTag>;
    using type = Dumux::PNMStokesCouplingManager<Traits>;
};

} // end namespace Dumux::Properties

#endif
